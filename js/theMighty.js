var $toate = $("#toate");
var $apartamente = $("#apartamente");
var $case_si_vile = $("#case_vile");

//toate conditii filtrari
var $pret_min = $("#pret_min");
var $pret_max = $("#pret_max");
var $suprafata_min = $("#suprafata_min");
var $suprafata_max = $("#suprafata_max");

//apartamente conditii filtrari
var $pret_min_ap = $("#pret_min_ap");
var $pret_max_ap = $("#pret_max_ap");
var $suprafata_min_ap = $("#suprafata_min_ap");
var $suprafata_max_ap = $("#suprafata_max_ap");

var jsonParams = {};

var rootUrl = 'https://demo.crmrebs.com';
var endpoint = '/api/public/property/';
var ajaxExecuting = false;
var proprietati = [];
var propertyTypes = [1, 3];
var renderer;

//template card
var templateCard = "" +
"<div class=\"col-md-4 card-proprietate\" >" +
	"<div class=\"card mb4 box-shadow\" >" +
		"<img class=\"card-img-top img-height\" src='{{resized_images.0}}' > "+
		"<div class=\"card-body\" >" +
				"<div class=\"row\" >" +
						"<div class=\"col-md-6\">" +
								"<span>Pret: {{price_sale}} </span>" +
						"</div>" +
						"<div class=\"col-md-6\">" +
						"</div>" +
				"</div>" +
				"<div class=\"row\" >" +
						"<div class=\"col-md-6\">" +
								"<span>Suprafata: {{surface_useable}}</span>" +
						"</div>" +
						"<div class=\"col-md-6\">" +
								"<a href=\"#\" class=\"detalii\">Detalii</a>" +
						"</div>" +
				"</div>" +
		"</div>" +
	"</div>" +
"</div>"

//---Toate proprietatile
var proprietateTemplate = templateCard;



function onSuccesRenderProperties(data) {
	proprietati = proprietati.concat(data.objects);

	$.each(data.objects, function (i, proprietate) {
		$toate.append(Mustache.render(proprietateTemplate, proprietate));
	});
}

function fetchProperties(propertyTypes, onSuccess) {
	var tip_tranzactie = $("#tip_tranzactie option:selected").text();

	endpoint = '/api/public/property/';
	proprietati = [];
	$toate.children().fadeOut(300, function () { $(this).remove(); });
	$apartamente.children().fadeOut(300, function () { $(this).remove(); });
	$case_si_vile.children().fadeOut(300, function () { $(this).remove(); });

	jsonParams = {};
	var price_sale__gte = $pret_min.val();
	var price_sale__lte = $pret_max.val();
	var surface_useable__gte = $suprafata_min.val();
	var surface_useable__lte = $suprafata_max.val();


	jsonParams.property_type__in = propertyTypes;


	if ($pret_min.val().length) {
		jsonParams.price_sale__gte = price_sale__gte;
	}

	if ($pret_max.val().length) {
		jsonParams.price_sale__lte = price_sale__lte;
	}
	if ($suprafata_min.val().length) {
		jsonParams.surface_useable__gte = surface_useable__gte;
	}

	if ($suprafata_max.val().length) {
		jsonParams.surface_useable__lte = surface_useable__lte;
	}

	if (tip_tranzactie == "Vanzare") {
		jsonParams.for_sale = 1;
	} else if (tip_tranzactie == "Inchiriere") {
		jsonParams.for_rent = 1;
	};

	getThemProperties(endpoint, jsonParams, onSuccess);
}//incheiere fetchProperties


$('#cauta_in_toate').on('click', function () {
	fetchProperties(propertyTypes, renderer);
});


function getThemProperties(currentEndpoint, jsonParams, onSuccess) {
	console.log(currentEndpoint);


	if (currentEndpoint != null && !ajaxExecuting) {
		ajaxExecuting = true;

		$.ajax({
			type: 'GET',
			url: rootUrl + currentEndpoint,
			data: jsonParams,
			traditional: true,
			success: function (data) {
				endpoint = data.meta.next;
				console.log("noul url va fi", endpoint);
				ajaxExecuting = false;
				onSuccess(data);
			},
			error: function () {
				alert("error loading toate");
			}
		});
	}

}



var scrollListener = function () {
	$(window).one("scroll", function () {
		if ($(window).scrollTop() >= $(document).height() - $(window).height() - 100) {
			getThemProperties(endpoint, jsonParams, renderer);
		}
		setTimeout(scrollListener, 500);
	});
};
$(document).ready(function () {
	scrollListener();
});
//sfarsit adauga urmatoarele 20 pe scroll



//--- Apartamente
var apartamentTemplate = templateCard;


function onSuccesRenderApartments(data) {
	proprietati = proprietati.concat(data.objects);

	$.each(data.objects, function (i, apartament) {
		$apartamente.append(Mustache.render(apartamentTemplate, apartament));
	});
}



//--- Case / Vile:
var casaTemplate = templateCard;

function onSuccesRenderHouses(data) {
	proprietati = proprietati.concat(data.objects);

	$.each(data.objects, function (i, casa) {
		$case_si_vile.append(Mustache.render(casaTemplate, casa));
	});
}

//tabs
$('.nav-item').on('click', function () {

	if (!$(this).hasClass("active")) {
		$('.nav-item').removeClass("active");
		$(this).addClass("active");
	}

	if ($(this).attr('id') == "nav-toate-tab") {
		propertyTypes = [1, 3];
		renderer = onSuccesRenderProperties;
	} else if ($(this).attr('id') == "nav-apartamente-tab") {
		propertyTypes = [1];
		renderer = onSuccesRenderApartments;
	} else if ($(this).attr('id') == "nav-case-tab") {
		propertyTypes = [3];
		renderer = onSuccesRenderHouses;
	}

	fetchProperties(propertyTypes, renderer);

});

renderer = onSuccesRenderProperties;
fetchProperties(propertyTypes, onSuccesRenderProperties);